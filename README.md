# Song Suggestion App
This is an app made for the fullstack challenge.


# Getting started
- Install docker in a Linux environment;
- Go to /scripts;
- Run ./infra-run.sh and ./app-run.sh;
- Open localhost;
- **You must not run the shell scripts/docker-compose commands as sudo**. Docker and NPM don't work well with sudo;
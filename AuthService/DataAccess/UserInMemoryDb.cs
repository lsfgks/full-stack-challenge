using System.Collections.Concurrent;
using System.Collections.Generic;
using AuthService.Domain;

namespace AuthService.DataAccess
{
    public class UserInMemoryDb : IUser
    {
        private readonly IDictionary<string, User> db = new ConcurrentDictionary<string, User>();

        public UserInMemoryDb()
        {
            Add(new User("admin", "admin"));
        }

        public void Add(User user)
        {
            db[user.Login] = user;
        }

        public User FindByLogin(string login) => db[login];
    }
}
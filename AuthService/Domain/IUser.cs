namespace AuthService.Domain
{
    public interface IUser
    {
        void Add(User user);

        User FindByLogin(string login);
    }
}
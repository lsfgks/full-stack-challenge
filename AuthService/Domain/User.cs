using System.Collections.Generic;

namespace AuthService.Domain
{
    public class User
    {
        public string Login { get; private set; }
        public string Password { get; private set; }

        public User(string login, string password)
        {
            Login = login;
            Password = password;
        }

        public bool PasswordMatches(string passwordToTest) => Password == passwordToTest;
    }
}
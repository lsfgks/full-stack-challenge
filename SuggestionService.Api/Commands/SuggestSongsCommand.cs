﻿using System.Collections.Generic;
using FluentValidation;
using MediatR;
using SuggestionService.Data.Models;

namespace SuggestionService.Api.Commands
{
    public class SuggestSongsCommand : IRequest<SuggestSongsResult>
    {
        public string Nickname { get; set; }
        public List<SuggestedSong> SuggestedSongs { get; set; }
    }

    public class SonggestSongsCommandValidator : AbstractValidator<SuggestSongsCommand>
    {
        public SonggestSongsCommandValidator()
        {
            RuleFor(ms => ms.Nickname).NotEmpty();
            RuleFor(ms => ms.Nickname).NotNull();
        }
    }
}

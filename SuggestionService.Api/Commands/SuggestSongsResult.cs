﻿using System;

namespace SuggestionService.Api.Commands
{
    public class SuggestSongsResult
    {
        public Guid SuggestionId { get; set; }
    }
}

using System.Collections.Generic;
using MediatR;
using SuggestionService.Data.Models;

namespace SuggestionService.Api.Queries
{
    public class GetAllSuggestionsQuery : IRequest<IEnumerable<Suggestion>>
    {
    }
}
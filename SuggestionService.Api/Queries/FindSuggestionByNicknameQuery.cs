using System.Collections.Generic;
using MediatR;
using SuggestionService.Data.Models;

namespace SuggestionService.Api.Queries
{
    public class FindSuggestionByNicknameQuery : IRequest<Suggestion>
    {
        public string Nickname { get; set; }
    }
}
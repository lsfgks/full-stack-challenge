import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatGridListModule,
  MatStepperModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule
} from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,

    // material modules
    MatGridListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    DragDropModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
  ],
  exports: [
    MatGridListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    DragDropModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
  ]
})
export class MaterialModule { }

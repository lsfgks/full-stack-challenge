import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongSuggestionComponent } from './song-suggestion/song-suggestion.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';
import { ResearchResourcesComponent } from './research-resources/research-resources.component';


@NgModule({
  declarations: [
    SongSuggestionComponent,
    AdminDashboardComponent,
    ResearchResourcesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ComponentsModule,
  ]
})
export class ViewsModule { }

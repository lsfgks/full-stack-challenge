import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SongListDataStore } from 'src/app/components/song-list/song-list.data-store';
import { Song } from 'src/app/components/song-list/song.model';
import { BaseComponent } from 'src/app/shared/base-component';
import { takeUntil, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import songsJson from '../../../data/spotify-top100-2018.json';
import { MatStepper, MatStep } from '@angular/material';
import { SwalService } from 'src/app/services/ui/swal.service.js';
import { BaseHttpService } from 'src/app/services/http/base.http.service.js';
import { SuggestionHttpService } from 'src/app/services/http/suggestion.http.service.js';

@Component({
  templateUrl: './song-suggestion.component.html'
})
export class SongSuggestionComponent extends BaseComponent implements OnInit {
  userInfo: FormGroup;
  selectedSongs: Song[];
  songsJson = songsJson;

  constructor(
    private fb: FormBuilder,
    private songListDataStore: SongListDataStore,
    private swal: SwalService,
    private suggestionHttpService: SuggestionHttpService
  ) {
    super();
  }

  ngOnInit(): void {
    this.userInfo = this.fb.group({
      nickname: ['', Validators.required]
    });

    this.observeSongSelection().subscribe();
  }

  onSongSelection(selectedSongs: Song[]): void {
    this.songListDataStore.setSelectedSongs(selectedSongs);
  }

  private observeSongSelection(): Observable<Song[]> {
    return this.songListDataStore.getSelectedSongs()
      .pipe(
        takeUntil(this.onDestroy$),
        tap(selectedSongs => {
          this.selectedSongs = selectedSongs;
        })
      );
  }

  removeSong(index: number): void {
    this.selectedSongs.splice(index, 1);
    this.songListDataStore.setSelectedSongs(this.selectedSongs);
  }

  submitSongs(stepper: MatStepper, step: MatStep): void {
    if (this.selectedSongs.length === 5) {
      this.suggestionHttpService.suggestSongs(this.userInfo.get('nickname').value, this.selectedSongs)
        .pipe(
          takeUntil(this.onDestroy$),
          tap(() => {
            step.completed = true;
            stepper.next();
          })
        ).subscribe();
    } else {
      this.swal.showSimpleAlert('<strong>Oops!</strong>', 'You need to rank five songs.');
    }
  }
}

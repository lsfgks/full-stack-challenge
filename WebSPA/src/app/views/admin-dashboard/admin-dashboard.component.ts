import { Component, OnInit } from '@angular/core';
import { SongRankingHttpService } from 'src/app/services/http/song-ranking.http.service';
import { RankedSong } from 'src/app/models/song-ranking.result';
import { BaseComponent } from 'src/app/shared/base-component';
import { takeUntil, tap } from 'rxjs/operators';

@Component({
  templateUrl: './admin-dashboard.component.html'
})
export class AdminDashboardComponent extends BaseComponent {
  rankedSongs: RankedSong[];

  constructor(
    private songRankingHttpService: SongRankingHttpService
  ) {
    super();
  }

  processRanking(): void {
    this.songRankingHttpService.rankSongs()
      .pipe(
        takeUntil(this.onDestroy$),
        tap(rankedSongs => this.rankedSongs = rankedSongs)
      )
      .subscribe();
  }
}

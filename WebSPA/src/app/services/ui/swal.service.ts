import Swal from 'sweetalert2';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SwalService {
  showSimpleAlert(title: string, html: string) {
    Swal.fire({
      title,
      html,
      showCloseButton: true,
      showConfirmButton: false,
      showClass: { popup: 'animated zoomIn faster' },
      hideClass: { popup: 'animated zoomOut faster' }
    });
  }
}

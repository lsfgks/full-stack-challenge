import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class BaseHttpService {
  constructor(private http: HttpClient) {}

  login(): Observable<{ token: string }> {
    // this login implementation is for demonstration purposes only
    return this.http.post<{ token: string }>(
      environment.authentication + 'User',
      {
        login: 'admin',
        password: 'admin'
      },
    );
  }
}

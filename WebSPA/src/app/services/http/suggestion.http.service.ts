import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { SuggestSongsCommand } from 'src/app/models/suggest-songs.command';
import { Song } from 'src/app/components/song-list/song.model';


@Injectable({
  providedIn: 'root'
})
export class SuggestionHttpService {

  constructor(private http: HttpClient) {}

  suggestSongs(nickname: string, suggestedSongs: Song[]): Observable<SuggestSongsCommand> {
    const headers = new HttpHeaders()
      .set('Authorization', `Bearer ${localStorage.getItem('token')}`);

    const command = new SuggestSongsCommand(nickname, suggestedSongs);


    return this.http.post<SuggestSongsCommand>(
      environment.gateway + 'Suggestions',
      command,
      { headers }
    );
  }
}

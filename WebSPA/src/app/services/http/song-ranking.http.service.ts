import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Song } from 'src/app/components/song-list/song.model';
import { RankedSong } from 'src/app/models/song-ranking.result';


@Injectable({
  providedIn: 'root'
})
export class SongRankingHttpService {

  constructor(private http: HttpClient) {}

  rankSongs(): Observable<RankedSong[]> {
    const headers = new HttpHeaders()
      .set('Authorization', `Bearer ${localStorage.getItem('token')}`);

    return this.http.get<RankedSong[]>(
      environment.gateway + 'SongRanking',
      { headers }
    );
  }
}

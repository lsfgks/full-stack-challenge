import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SongListComponent } from './song-list/song-list.component';
import { SongRankingComponent } from './song-ranking/song-ranking.component';
import { UserRankingComponent } from './user-ranking/user-ranking.component';
import { SharedModule } from '../shared/shared.module';
import { SongSortingComponent } from './song-sorting/song-sorting.component';



@NgModule({
  declarations: [
    SongListComponent,
    SongRankingComponent,
    UserRankingComponent,
    SongSortingComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    SongListComponent,
    SongRankingComponent,
    UserRankingComponent,
    SongSortingComponent,
  ]
})
export class ComponentsModule { }

import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { RankedSong } from 'src/app/models/song-ranking.result';

@Component({
  selector: 'app-song-ranking',
  templateUrl: './song-ranking.component.html',
  styleUrls: ['./song-ranking.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SongRankingComponent implements OnInit {
  @Input() rankedSongs: RankedSong[];

  constructor() { }

  ngOnInit(): void {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SongSortingComponent } from './song-sorting.component';

describe('SongSortingComponent', () => {
  let component: SongSortingComponent;
  let fixture: ComponentFixture<SongSortingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SongSortingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SongSortingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

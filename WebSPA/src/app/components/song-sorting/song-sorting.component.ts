import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Song } from '../song-list/song.model';
import { moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-song-sorting',
  templateUrl: './song-sorting.component.html'
})
export class SongSortingComponent implements OnInit {
  @Input() selectedSongs: Song[];
  @Output() removeSong = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.selectedSongs, event.previousIndex, event.currentIndex);
  }

  removeSongFromSelectedList(songIndex: number): void {
    this.removeSong.next(songIndex);
  }
}

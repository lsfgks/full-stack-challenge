import { BehaviorSubject, Observable } from 'rxjs';
import { Song } from './song.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SongListDataStore {
  selectedSongs$ = new BehaviorSubject<Song[]>(null);

  getSelectedSongs(): Observable<Song[]> {
    return this.selectedSongs$;
  }

  setSelectedSongs(selectedSongs: Song[]): void {
    this.selectedSongs$.next(selectedSongs);
  }
}

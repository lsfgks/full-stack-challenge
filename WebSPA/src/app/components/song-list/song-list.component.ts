import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import songsJson from '../../../data/spotify-top100-2018.json';
import { Song } from './song.model.js';
import { SelectionType, DatatableComponent } from '@swimlane/ngx-datatable';
import { SwalService } from 'src/app/services/ui/swal.service.js';

@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html'
})
export class SongListComponent implements OnInit {
  @Output() songSelection = new EventEmitter<Song[]>();
  @Input() selectedSongs: Song[];
  songList: Song[] = songsJson;
  datatable: Partial<DatatableComponent>;
  reachedSongLimit = false;

  enum = {
    SelectionType,
  };

  constructor(private swal: SwalService) {}

  ngOnInit(): void {
    this.datatable = this.createTable();
  }

  filterDatatable(term: string): void {
    term = term.toLowerCase();

    this.songList = songsJson.filter((song: Song) => {
      return song.name.toString().toLowerCase().includes(term)
          || song.genre.toString().toLowerCase().includes(term)
          || song.artists.toString().toLowerCase().includes(term);
    });
  }

  onSelect(selectedSongs: Song[], datatable: DatatableComponent): void {
    if (selectedSongs.length <= 5) {
      this.selectedSongs = datatable.selected;
      this.songSelection.next(this.selectedSongs);
    } else {
      this.selectedSongs.splice(this.selectedSongs.length - 1, 1);
      this.swal.showSimpleAlert('<strong>Oops!</strong>', 'You can only select five songs.');
    }
  }

  private createTable(): Partial<DatatableComponent> {
    return {
      columns: [{ prop: 'name' }, { prop: 'artists' }, { prop: 'genre' }],
      messages: {
        emptyMessage: `No songs found. Try another search term!`
      }
    };
  }
}

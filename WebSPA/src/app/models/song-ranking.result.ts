export class RankedSong {
  spotifyId: string;
  songName: string;
  artist: string;
  timesChosen: string;
}

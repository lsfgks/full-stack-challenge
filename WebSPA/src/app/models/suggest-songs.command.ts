import { Song } from '../components/song-list/song.model';

class SongDto {
  spotifyId: string;
  name: string;
  artist: string;
  placement: number;
}

export class SuggestSongsCommand {
  nickname: string;
  suggestedSongs: SongDto[];

  constructor(nickname: string, suggestedSongs: Song[]) {
    this.nickname = nickname;
    this.suggestedSongs = this.toSongDto(suggestedSongs);
  }

  private toSongDto(songs: Song[]): SongDto[] {
    return songs.map((song, index) => {
      const songDto = new SongDto();

      songDto.spotifyId = song.id;
      songDto.name = song.name;
      songDto.artist = song.artists;
      songDto.placement = index + 1;
      return songDto;
    });
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SongSuggestionComponent } from './views/song-suggestion/song-suggestion.component';
import { AdminDashboardComponent } from './views/admin-dashboard/admin-dashboard.component';
import { ResearchResourcesComponent } from './views/research-resources/research-resources.component';


const routes: Routes = [
  { path: '', component: SongSuggestionComponent },
  { path: 'admin', component: AdminDashboardComponent },
  { path: 'resources', component: ResearchResourcesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

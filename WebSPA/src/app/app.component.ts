import { Component, OnInit } from '@angular/core';
import { BaseHttpService } from './services/http/base.http.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  template: `

    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <h1>CI&T Full-Stack Challenge</h1>
        <span class="menu-spacer"></span>
        <div>
          <a mat-button [routerLink]="'/'">Home</a>
          <a mat-button [routerLink]="'/admin'">Admin panel</a>
          <a mat-button [routerLink]="'/resources'">Researching resources</a>
        </div>
      </mat-toolbar-row>
    </mat-toolbar>
    <div class="main-container">
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent implements OnInit {

  constructor(private auth: BaseHttpService) {}

  ngOnInit(): void {
    this.auth.login()
      .pipe(
        tap(res => localStorage.setItem('token', res.token))
      )
      .subscribe();
  }
}

export const environment = {
  production: false,
  authentication: 'http://localhost:6060/api/',
  gateway: 'http://localhost:8099/api/',
};

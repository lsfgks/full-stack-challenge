CREATE USER song_suggestion_admin WITH ENCRYPTED PASSWORD 'pass';

-- payments
CREATE DATABASE "SongSuggestionApp";
GRANT ALL PRIVILEGES ON DATABASE "SongSuggestionApp" TO song_suggestion_admin;

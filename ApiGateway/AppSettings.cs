namespace ApiGateway
{
    public class AppSettings
    {
        public string[] AllowedOrigins { get; set; }
    }
}
namespace SongRankingService.Api.Queries.Dto
{
    public class RankedSongDto
    {
        public string SpotifyId { get; set; }
        public string SongName { get; set; }
        public string Artist { get; set; }
        public int TimesChosen { get; set; }
    }
}
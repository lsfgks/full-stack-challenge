using System;
using System.Collections.Generic;

namespace SongRankingService.Api.Queries.Dto
{
    public class SuggestionDto
    {
        public Guid Id { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public string Nickname { get; set; }
        
        public virtual List<SuggestedSongsDto> SuggestedSongs { get; set; }
    }
}
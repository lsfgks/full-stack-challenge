using System.Collections.Generic;
using MediatR;
using SongRankingService.Api.Queries.Dto;

namespace SongRankingService.Api.Queries
{
    public class GetSongsOrderByMostVotedQuery : IRequest<IEnumerable<RankedSongDto>>
    {
    }
}
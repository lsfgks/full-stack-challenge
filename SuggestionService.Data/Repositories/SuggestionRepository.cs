using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService.Data.Repositories
{
    public class SuggestionRepository : ISuggestionRepository
    {
        private readonly SuggestionContext context;

        public SuggestionRepository(SuggestionContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Suggestion> Add(Suggestion suggestion)
        {
            await context.Suggestions.AddAsync(suggestion);
            await context.SuggestedSongs.AddRangeAsync(suggestion.SuggestedSongs);
            context.SaveChanges();
            return suggestion;
        }

        public async Task<List<Suggestion>> ListAll()
        {
            return await context
                .Suggestions
                .ToListAsync();
        }

        public async Task<Suggestion> FindOneByNickname(string nickname)
        {
            return await context
                .Suggestions
                .FirstOrDefaultAsync(s => s.Nickname.Equals(nickname));
        }

        public async Task<Suggestion> FindById(Guid id)
        {
            return await context.Suggestions
                .FirstOrDefaultAsync(s => s.Id == id);
        }
    }
}
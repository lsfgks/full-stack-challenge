using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService.Data.Repositories
{
    public class SuggestedSongRepository : ISuggestedSongRepository
    {
        private readonly SuggestionContext context;

        public SuggestedSongRepository(SuggestionContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<SuggestedSong> Add(SuggestedSong suggestedSong)
        {
            await context.SuggestedSongs.AddAsync(suggestedSong);
            context.SaveChanges();
            return suggestedSong;
        }
        
        public async Task<IEnumerable<SuggestedSong>> ListAll()
        {
            var songs = await context
                .SuggestedSongs
                .ToListAsync();

            return songs;
        }

        public async Task<IEnumerable<SuggestedSong>> FindBySuggestionId(Guid suggestionId)
        {
            var songs = await context.SuggestedSongs
                .Where(s => s.SuggestionId == suggestionId).ToListAsync();
            return songs;
        }
    }
}
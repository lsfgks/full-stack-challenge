using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuggestionService.Data.Models;

namespace SuggestionService.Data.Repositories.Interfaces
{
    public interface ISuggestedSongRepository
    {
        Task<SuggestedSong> Add(SuggestedSong suggestedSong);
        Task<IEnumerable<SuggestedSong>> FindBySuggestionId(Guid suggestionId);
        Task<IEnumerable<SuggestedSong>> ListAll();
    }
}
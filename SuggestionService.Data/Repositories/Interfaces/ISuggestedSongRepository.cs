using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuggestionService.Data.Models;

namespace SuggestionService.Data.Repositories.Interfaces
{
    public interface ISuggestionRepository
    {
        Task<Suggestion> Add(Suggestion suggestion);

        Task<List<Suggestion>> ListAll();

        Task<Suggestion> FindOneByNickname(string nickname);
        
        Task<Suggestion> FindById(Guid id);
    }
}
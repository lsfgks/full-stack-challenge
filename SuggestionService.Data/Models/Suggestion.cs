using System;
using System.Collections.Generic;

namespace SuggestionService.Data.Models
{
    public class Suggestion
    {
        public Suggestion()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
        }
        
        public Guid Id { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public string Nickname { get; set; }
        
        public virtual List<SuggestedSong> SuggestedSongs { get; set; }
    }
}

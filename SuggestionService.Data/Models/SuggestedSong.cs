using System;
using System.Text.Json.Serialization;

namespace SuggestionService.Data.Models
{
    public class SuggestedSong
    {
        public SuggestedSong()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
        }
        
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid SuggestionId { get; set; }

        public string SpotifyId { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public int Placement { get; set; }
        
        [JsonIgnore]
        public virtual Suggestion Suggestion { get; set; }
    }
}
using Microsoft.EntityFrameworkCore;
using SuggestionService.Data.Models.Configurations;

namespace SuggestionService.Data.Models
{
    public class SuggestionContext : DbContext
    {
        public SuggestionContext() {}
        
        public SuggestionContext(DbContextOptions<SuggestionContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new SuggestionConfiguration());
            modelBuilder.ApplyConfiguration(new SuggestedSongConfiguration());
        }
        
        public DbSet<Suggestion> Suggestions { get; set; }
        public DbSet<SuggestedSong> SuggestedSongs { get; set; }
    }
}
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SuggestionService.Data.Models.Configurations
{
    public class SuggestedSongConfiguration : IEntityTypeConfiguration<SuggestedSong>
    {
        public void Configure(EntityTypeBuilder<SuggestedSong> builder)
        {
            builder.HasKey(prop => prop.Id);
            builder.Property(prop => prop.CreatedOn)
                .HasColumnType("TIMESTAMP(0)")
                .IsRequired();

            builder.Property(prop => prop.Placement)
                .IsRequired();
        }
    }
}
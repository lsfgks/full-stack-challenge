using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using UserRankingService.Api.Queries;

namespace UserRankingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRankingController : ControllerBase
    {
        private readonly IMediator bus;
        
        public UserRankingController(IMediator bus)
        {
            this.bus = bus;
        }
        
        [HttpGet]
        public async Task<ActionResult> GetRankingByNickname([FromRoute]GetRankingByNicknameQuery query)
        {
            var result = await bus.Send(query);
            return new JsonResult(result);
        }
    }
}
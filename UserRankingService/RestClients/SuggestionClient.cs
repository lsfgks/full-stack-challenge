using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Polly;
using RestEase;
using Steeltoe.Common.Discovery;
using UserRankingService.Api.Queries.Dto;


namespace UserRankingService.RestClients
{
    public interface ISuggestionClient
    {
        [Get]
        Task<IEnumerable<SuggestionDto>> GetAllSuggestions();
    }
    
    public class SuggestionClient : ISuggestionClient
    {
        private readonly ISuggestionClient client;
        
        private static readonly Policy RetryPolicy = Policy
            .Handle<HttpRequestException>()
            .WaitAndRetryAsync(retryCount: 3, sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(3));
        
        public SuggestionClient(IConfiguration configuration, IDiscoveryClient discoveryClient)
        {
            var handler = new DiscoveryHttpClientHandler(discoveryClient);
            var httpClient = new HttpClient(handler, false)
            {
                BaseAddress = new Uri(configuration.GetValue<string>("SuggestionServiceUri"))
            };
            client = RestClient.For<ISuggestionClient>(httpClient);
        }

        public Task<IEnumerable<SuggestionDto>> GetAllSuggestions()
        {
            return RetryPolicy.ExecuteAsync(async () => await client.GetAllSuggestions());
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using UserRankingService.Api.Queries.Dto;
using UserRankingService.Domain;

namespace UserRankingService.RestClients
{
    public class SuggestedSongService : ISuggestedSongService
    {
        private readonly ISuggestedSongClient suggestedSongClient;

        public SuggestedSongService(ISuggestedSongClient suggestedSongClient)
        {
            this.suggestedSongClient = suggestedSongClient;
        }

        public async Task<IEnumerable<SuggestedSongsDto>> GetAllSuggestedSongs()
        {
            var result = await suggestedSongClient.GetAllSuggestedSongs();
            return result;
        }
    }
}
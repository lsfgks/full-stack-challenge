using System.Collections.Generic;
using System.Threading.Tasks;
using UserRankingService.Api.Queries.Dto;
using UserRankingService.Domain;

namespace UserRankingService.RestClients
{
    public class SuggestionService : ISuggestionService
    {
        private readonly ISuggestionClient suggestionClient;

        public SuggestionService(ISuggestionClient suggestionClient)
        {
            this.suggestionClient = suggestionClient;
        }

        public async Task<IEnumerable<SuggestionDto>> GetAllSuggestions()
        {
            var result = await suggestionClient.GetAllSuggestions();
            return result;
        }
    }
}
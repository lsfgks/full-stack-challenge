using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using UserRankingService.Api.Queries;
using UserRankingService.Api.Queries.Dto;
using UserRankingService.Domain;

namespace UserRankingService.Queries
{
    public class GetRankingByNicknameQueryHandler : IRequestHandler<GetRankingByNicknameQuery, IEnumerable<RankedNicknameDto>>
    {
        private readonly ISuggestedSongService suggestedSongService;
        private readonly ISuggestionService suggestionService;
        
        public GetRankingByNicknameQueryHandler(
            ISuggestedSongService suggestedSongService,
            ISuggestionService suggestionService
        )
        {
            this.suggestedSongService = suggestedSongService;
            this.suggestionService = suggestionService;
        }
        
        public async Task<IEnumerable<RankedNicknameDto>> Handle(GetRankingByNicknameQuery query, CancellationToken cancellationToken)
        {
            /**
             * TODO: Listen to the top 5 songs microservice to calculate user ranking using it's result.
             */

            return null;
        }
        
    }
}
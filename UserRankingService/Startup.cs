using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Steeltoe.Discovery.Client;
using UserRankingService.Domain;
using UserRankingService.RestClients;

namespace UserRankingService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDiscoveryClient(Configuration);
            services.AddControllers();
            services.AddMediatR();
            
            services.AddScoped<ISuggestionService, SuggestionService>();
            services.AddScoped<ISuggestionClient, SuggestionClient>();
            services.AddScoped<ISuggestedSongService, SuggestedSongService>();
            services.AddScoped<ISuggestedSongClient, SuggestedSongClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseDiscoveryClient();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using UserRankingService.Api.Queries.Dto;

namespace UserRankingService.Domain
{
    public interface ISuggestionService
    {
        Task<IEnumerable<SuggestionDto>> GetAllSuggestions();
    }
}
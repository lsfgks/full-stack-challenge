using System.Collections.Generic;
using System.Threading.Tasks;
using UserRankingService.Api.Queries.Dto;

namespace UserRankingService.Domain
{
    public interface ISuggestedSongService
    {
        Task<IEnumerable<SuggestedSongsDto>> GetAllSuggestedSongs();
    }
}
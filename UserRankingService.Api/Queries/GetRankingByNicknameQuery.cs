
using System.Collections.Generic;
using MediatR;
using UserRankingService.Api.Queries.Dto;

namespace UserRankingService.Api.Queries
{
    public class GetRankingByNicknameQuery : IRequest<IEnumerable<RankedNicknameDto>>
    {
        
    }
}
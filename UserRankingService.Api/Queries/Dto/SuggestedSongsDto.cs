using System;

namespace UserRankingService.Api.Queries.Dto
{
    public class SuggestedSongsDto
    {
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid SuggestionId { get; set; }

        public string SpotifyId { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public int Placement { get; set; }
    }
}
namespace UserRankingService.Api.Queries.Dto
{
    public class RankedNicknameDto
    {
        public string Nickname { get; set; }
        public int Contributions { get; set; }
    }
}
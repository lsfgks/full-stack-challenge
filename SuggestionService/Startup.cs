using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Steeltoe.Discovery.Client;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDiscoveryClient(Configuration);
            services.AddControllers();
            
            services.AddMediatR();
            services.AddDbContext<SuggestionContext>(
                options => options.UseNpgsql(Configuration.GetConnectionString("PgConnection"))
            );
            
            services.AddScoped<ISuggestionRepository, SuggestionRepository>();
            services.AddScoped<ISuggestedSongRepository, SuggestedSongRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, SuggestionContext dataContext)
        {
            dataContext.Database.Migrate();
            
            app.UseRouting();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            // app.UseInitializer();
            app.UseDiscoveryClient();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
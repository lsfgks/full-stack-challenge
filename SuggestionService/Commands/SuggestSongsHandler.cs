using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuggestionService.Api.Commands;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService.Commands
{
    public class SuggestSongsHandler : IRequestHandler<SuggestSongsCommand, SuggestSongsResult>
    {
        private readonly ISuggestionRepository suggestionRepository;
        // private readonly ISuggestedSongRepository suggestedSongRepository;
        
        public SuggestSongsHandler(
            ISuggestionRepository suggestionRepository
            // ISuggestedSongRepository suggestedSongRepository
        )
        {
            this.suggestionRepository = suggestionRepository;
            // this.suggestedSongRepository = suggestedSongRepository;
        }
        
        public async Task<SuggestSongsResult> Handle(SuggestSongsCommand request, CancellationToken cancellationToken)
        {
            var suggestion = await suggestionRepository.Add(ToSuggestion(request));

            // var suggestedSongs = request.SuggestedSongs
            //     .Select(suggestedSong => suggestedSongRepository.Add(suggestedSong));

            return new SuggestSongsResult()
            {
                SuggestionId = suggestion.Id
            };
        }

        private Suggestion ToSuggestion(SuggestSongsCommand cmd)
        {
            return new Suggestion()
            {
                Nickname = cmd.Nickname,
                SuggestedSongs = cmd.SuggestedSongs
            };
        }
    }
}
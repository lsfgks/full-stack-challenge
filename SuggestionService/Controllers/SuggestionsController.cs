using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SuggestionService.Api.Commands;
using SuggestionService.Api.Queries;

namespace SuggestionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuggestionsController : ControllerBase
    {
        private readonly IMediator mediator;

        public SuggestionsController(IMediator mediator)
        {
            this.mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet]
        public async Task<ActionResult> GetAllSuggestions()
        {
            var result = await mediator.Send(new GetAllSuggestionsQuery());
            return new JsonResult(result);
        }

        [HttpPost]
        public async Task<ActionResult> SuggestSongs([FromBody] SuggestSongsCommand command)
        {
            var result = await mediator.Send(command);
            return new JsonResult(result);
        }
    }
}
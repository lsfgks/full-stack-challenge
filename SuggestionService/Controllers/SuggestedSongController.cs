using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SuggestionService.Api.Queries;

namespace SuggestionService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuggestedSongController
    {
        private readonly IMediator mediator;

        public SuggestedSongController(IMediator mediator)
        {
            this.mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        
        [HttpGet]
        public async Task<ActionResult> GetAllSuggestedSongs()
        {
            var result = await mediator.Send(new GetAllSuggestedSongsQuery());                        
            return new JsonResult(result);
        }
        
    }
}
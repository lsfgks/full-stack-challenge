using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuggestionService.Api.Queries;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService.Queries
{
    public class FindByNicknameHandler : IRequestHandler<FindSuggestionByNicknameQuery, Suggestion>
    {
        private readonly ISuggestionRepository suggestionRepository;
        private readonly ISuggestedSongRepository suggestedSongRepository;

        public FindByNicknameHandler(
            ISuggestionRepository suggestionRepository,
            ISuggestedSongRepository suggestedSongRepository
        )
        {
            this.suggestionRepository = suggestionRepository ?? throw new ArgumentNullException(nameof(suggestionRepository));
            this.suggestedSongRepository = suggestedSongRepository ?? throw new ArgumentNullException(nameof(suggestedSongRepository));
        }
        
        public async Task<Suggestion> Handle(FindSuggestionByNicknameQuery query, CancellationToken cancellationToken)
        {
            var suggestion = await suggestionRepository.FindOneByNickname(query.Nickname);

            if (suggestion != null)
            {
                suggestion.SuggestedSongs = new List<SuggestedSong>();
                suggestion.SuggestedSongs.AddRange(suggestedSongRepository.FindBySuggestionId(suggestion.Id).Result);
                return suggestion;
            }
            else
            {
                return null;
            }
        }
        
        
    }
}
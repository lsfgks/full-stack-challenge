using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuggestionService.Api.Queries;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService.Queries
{
    public class GetAllSuggestedSongsHandler : IRequestHandler<GetAllSuggestedSongsQuery, IEnumerable<SuggestedSong>>
    {
        private readonly ISuggestedSongRepository suggestedSongRepository;

        public GetAllSuggestedSongsHandler(
            ISuggestedSongRepository suggestedSongRepository
        )
        {
            this.suggestedSongRepository = suggestedSongRepository ?? throw new ArgumentNullException(nameof(suggestedSongRepository));
        }
        
        public async Task<IEnumerable<SuggestedSong>> Handle(GetAllSuggestedSongsQuery query, CancellationToken cancellationToken)
        {
            var songs = await suggestedSongRepository.ListAll();
            return songs;
        }
        
        
    }
}
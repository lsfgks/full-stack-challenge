using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SuggestionService.Api.Queries;
using SuggestionService.Data.Models;
using SuggestionService.Data.Repositories.Interfaces;

namespace SuggestionService.Queries
{
    public class GetAllSuggestionsHandler : IRequestHandler<GetAllSuggestionsQuery, IEnumerable<Suggestion>>
    {
        private readonly ISuggestionRepository suggestionRepository;

        public GetAllSuggestionsHandler(
            ISuggestionRepository suggestionRepository
        )
        {
            this.suggestionRepository = suggestionRepository ?? throw new ArgumentNullException(nameof(suggestionRepository));
        }
        
        public async Task<IEnumerable<Suggestion>> Handle(GetAllSuggestionsQuery query, CancellationToken cancellationToken)
        {
            var songs = await suggestionRepository.ListAll();
            return songs;
        }
    }
}
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SongRankingService.Api.Queries;

namespace SongRankingService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SongRankingController : ControllerBase
    {
        private readonly IMediator bus;
        
        public SongRankingController(IMediator bus)
        {
            this.bus = bus;
        }
        
        [HttpGet]
        public async Task<ActionResult> GetSongsOrderByMostVoted([FromRoute]GetSongsOrderByMostVotedQuery query)
        {
            var result = await bus.Send(query);
            return new JsonResult(result);
        }
    }
}
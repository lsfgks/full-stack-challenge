using System.Collections.Generic;
using System.Threading.Tasks;
using SongRankingService.Api.Queries.Dto;

namespace SongRankingService.Domain
{
    public interface ISuggestedSongService
    {
        Task<IEnumerable<SuggestedSongsDto>> GetAll();
    }
}
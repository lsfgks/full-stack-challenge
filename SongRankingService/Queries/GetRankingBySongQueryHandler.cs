using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SongRankingService.Api.Queries;
using SongRankingService.Api.Queries.Dto;
using SongRankingService.Domain;

namespace SongRankingService.Queries
{
    public class GetRankingBySongQueryHandler : IRequestHandler<GetSongsOrderByMostVotedQuery, IEnumerable<RankedSongDto>>
    {
        private readonly ISuggestedSongService suggestedSongService;

        public GetRankingBySongQueryHandler(ISuggestedSongService suggestedSongService)
        {
            this.suggestedSongService = suggestedSongService;
        }
        
        public async Task<IEnumerable<RankedSongDto>> Handle(GetSongsOrderByMostVotedQuery query, CancellationToken cancellationToken)
        {
            var allSuggestions = await suggestedSongService.GetAll();
            
            var topFive = allSuggestions
                .GroupBy(a => a.SpotifyId)
                .OrderByDescending(gp => gp.Count())
                .Select(g => new { Result = g.First(), Count = g.Count() })
                .Take(5)
                .Select(g => new RankedSongDto()
                {
                    TimesChosen = g.Count,
                    Artist = g.Result.Artist,
                    SongName = g.Result.Name,
                    SpotifyId = g.Result.SpotifyId
                })
                .ToList();

            return topFive;
        }
    }
}
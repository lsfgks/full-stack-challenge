using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Polly;
using RestEase;
using SongRankingService.Api.Queries.Dto;
using Steeltoe.Common.Discovery;


namespace SongRankingService.RestClients
{
    public interface ISuggestedSongClient
    {
        [Get]
        Task<IEnumerable<SuggestedSongsDto>> GetAllSuggestions();
    }
    
    public class SuggestedSongClient : ISuggestedSongClient
    {
        private readonly ISuggestedSongClient client;
        
        private static readonly Policy RetryPolicy = Policy
            .Handle<HttpRequestException>()
            .WaitAndRetryAsync(retryCount: 3, sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(3));
        
        public SuggestedSongClient(IConfiguration configuration, IDiscoveryClient discoveryClient)
        {
            var handler = new DiscoveryHttpClientHandler(discoveryClient);
            var httpClient = new HttpClient(handler, false)
            {
                BaseAddress = new Uri(configuration.GetValue<string>("SuggestedSongServiceUrl"))
            };
            client = RestClient.For<ISuggestedSongClient>(httpClient);
        }

        public Task<IEnumerable<SuggestedSongsDto>> GetAllSuggestions()
        {
            return RetryPolicy.ExecuteAsync(async () => await client.GetAllSuggestions());
        }
    }
}
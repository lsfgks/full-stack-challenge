using System.Collections.Generic;
using System.Threading.Tasks;
using SongRankingService.Api.Queries.Dto;
using SongRankingService.Domain;

namespace SongRankingService.RestClients
{
    public class SuggestedSongService : ISuggestedSongService
    {
        private readonly ISuggestedSongClient suggestedSongClient;

        public SuggestedSongService(ISuggestedSongClient suggestedSongClient)
        {
            this.suggestedSongClient = suggestedSongClient;
        }

        public async Task<IEnumerable<SuggestedSongsDto>> GetAll()
        {
            var result = await suggestedSongClient.GetAllSuggestions();
            return result;
        }
    }
}